# Bases de Datos

¡Hola! Soy Javier, estudiante de grado superior de Desarrollo de Aplicaciones Web (DAW) en el instituto IES la Senia.

## Introducción a las Bases de Datos

Las bases de datos son fundamentales en el desarrollo de aplicaciones web, ya que nos permiten almacenar y gestionar grandes cantidades de información de manera eficiente y estructurada.

## Ejercicios que voy realizando

* **[Consultas Instagram](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_Instagram)**
* **[Recapitulacion](https://gitlab.com/jaavii_04/basededatos/-/blob/main/Recapitulacion/RECAP.md)**
* **[Consultas básicas](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_b%C3%A1sicas_sanitat)**
* **[Consultas básicas II](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_basicas_II)**
* **[Consultas basicas Join](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_basicas_Join)**
* **[Consultas basicas sanitat](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_basicas_sanitat)**
* **[Consultas funciones](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_funciones)**