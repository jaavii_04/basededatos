**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_b%C3%A1sicas_sanitat)**

# Reto 1: Consultas básicas

Empezaremos 

## Query 1: Muestre los hospitales existentes (número, nombre y teléfono).
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD, NOM, TELEFON
FROM HOSPITAL;
```

**[Enlace el archivo de la consulta](./query/query1.sql)**

## Query 2: Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.
Primero vamos a seleccionar de la tabla *HOSPITAL* las 3 columnas de esa misma tabla que son:  *HOSPITAL_COD* , *NOM* y *TELEFON*. Despues vamos a seleccionar la tabla *HOSPITAL* de la base de datos *sanitat*. Ahora vamos a hacer la condicion con el *WHERE* que vamos a coger la segunda letra del nombre y vamos a ver si es igual a la **A** para saber cuales tienen en la posicion 2 la **A**.

```sql
SELECT sanitat.HOSPITAL.HOSPITAL_COD ,sanitat.HOSPITAL.NOM ,sanitat.HOSPITAL.TELEFON
FROM sanitat.HOSPITAL
WHERE SUBSTRING(sanitat.HOSPITAL.NOM, 2, 1) LIKE "A";
```

**[Enlace el archivo de la consulta](./query/query2.sql)**

## Query 3: Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.

Vamos a seleccionar de la tabla *PLANTILLA* las columnas *hospital_cod* , *sala_cod* , *empleat_no* y *cognom*. Y vamos a selecionar la tabla *PLANTILLA* de la base de datos *sanitat*. Aqui no haremos nunguna condicion ya que vamos a mostrar todo lo que el contenido de la tabla.
```sql
SELECT sanitat.PLANTILLA.hospital_cod, sala_cod, empleat_no, cognom
FROM sanitat.PLANTILLA;
```

**[Enlace el archivo de la consulta](./query/query3.sql)**

## Query 4: Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.

Vamos a seleccionar de la tabla *PLANTILLA* las columnas *hospital_cod* , *sala_cod* , *empleat_no* y *cognom*. Y vamos a selecionar la tabla *PLANTILLA* de la base de datos *sanitat*.Vamos a realizar con el *WHERE* la condicion de mostrar solo los que no son **N** que es el turno de noche por esto ponemos **!=** que es lo contrario por eso salen todas menos las de turno de noche.
```sql
SELECT sanitat.PLANTILLA.hospital_cod, sala_cod, empleat_no, cognom
FROM sanitat.PLANTILLA
WHERE TORN != "N";
```

**[Enlace el archivo de la consulta](./query/query4.sql)**

## Query 5: Muestre a los enfermos nacidos en 1960.

Vamos a seleccionar de la tabla *MALALT* todas las columnas aqui no ponemos la tabla ya que solo es una tabla la vamos a vamos a seleccionar en el *FROM*, si tubieramos dos si que especificariamos la tabla que queremos con por ejemplo _MALALT.*_. Ahora vamos a hacer la condicion que en este caso es que queremos los que la fecha de nacimiento sea = a 1960, asique vamos a obtener el año con la funcion **YEAR** de la columna *DATA_NAIX* y ahi lo igualamos al año 1960.
```sql
SELECT *
FROM sanitat.MALALT
WHERE YEAR(MALALT.DATA_NAIX) = 1960;
```

**[Enlace el archivo de la consulta](./query/query5.sql)**

## Query 6: Muestre a los enfermos nacidos a partir del año 1960

Vamos a seleccionar de la tabla *MALALT* todas las columnas aqui no ponemos la tabla ya que solo es una tabla la vamos a vamos a seleccionar en el *FROM*, si tubieramos dos si que especificariamos la tabla que queremos con por ejemplo _MALALT.*_. Ahora vamos a hacer la condicion que en este caso es que queremos los que la fecha de nacimiento sea = o mayor a 1960.
```sql
SELECT *
FROM sanitat.MALALT
WHERE YEAR(MALALT.DATA_NAIX) >= 1960;
```

**[Enlace el archivo de la consulta](./query/query6.sql)**
