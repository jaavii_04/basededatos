**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/tree/main/SubConsultas)**
# Reto 2.2: Consultas con subconsultas

## Query 1:
Primero seleccionamos los campos Playlist.PlaylistId, Playlist.Name, t.TrackId, t.Name, t.AlbumId, Album.Title, t.UnitPrice vamos a unir las tablas con el Join y luego haremos un WHERE para que el nombre empieze por M y las pistas sean las 3 mas baratas de cada album.
```sql
SELECT Playlist.PlaylistId, Playlist.Name, t.TrackId, t.Name, t.AlbumId, Album.Title, t.UnitPrice
FROM Playlist
JOIN PlaylistTrack
    ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
JOIN Track AS t
    ON PlaylistTrack.TrackId = t.TrackId
JOIN Album
    ON t.AlbumId = Album.AlbumId
WHERE Playlist.Name LIKE 'M%'
    AND (
        SELECT COUNT(*)
        FROM Track
        WHERE Track.AlbumId = Album.AlbumId
        AND UnitPrice <= t.UnitPrice
    ) <= 3
ORDER BY Album.Title, t.UnitPrice;
```
**[Enlace el archivo de la consulta 1](./query/query1.sql)**

## Query 2:
Vamos a mostrar los acristas que tienen canciones de mayor de 5 minutos pero como esta en milisegundos ponemos 300000 que serian 5 minutos. 
```sql
SELECT DISTINCT Artist.ArtistId, Artist.Name
FROM Artist
WHERE Artist.ArtistId IN (
SELECT DISTINCT Artist.ArtistId
FROM Artist
JOIN  Album
    ON Artist.ArtistId = Album.ArtistId
JOIN Track
    ON Album.AlbumId = Track.AlbumId
WHERE
    Track.Milliseconds > 300000
);
```
**[Enlace el archivo de la consulta 2](./query/query2.sql)**

## Query 3:
Mostraremos el nombre y apellido de los empreleados los cuales tengan clientes asignados 
```sql
SELECT Employee.FirstName, Employee.LastName
FROM Employee
WHERE Employee.EmployeeId IN (
    SELECT DISTINCT Customer.SupportRepId
    FROM Customer
);
```
**[Enlace el archivo de la consulta 3](./query/query3.sql)**

## Query 4:
Vamos a mostrar todas las canciones que no han tenido ninguna venta 
```sql
SELECT Track.TrackId, Track.Name
FROM Track
WHERE Track.TrackId NOT
IN (
    SELECT InvoiceLine.TrackId
    FROM InvoiceLine
);
```
**[Enlace el archivo de la consulta 4](./query/query4.sql)**

## Query 5:
Vamos a listar los empleados al lado de sus subordinados.
```sql
SELECT e.EmployeeId, e.FirstName, e.LastName, e.ReportsTo, supervisor.nombre
FROM Employee e
JOIN (SELECT EmployeeId, CONCAT(FirstName, ' ', LastName) AS nombre
    FROM Employee) AS supervisor
    ON e.ReportsTo = supervisor.EmployeeId
ORDER BY e.ReportsTo, e.EmployeeId;
```
**[Enlace el archivo de la consulta 5](./query/query5.sql)**

## Query 6:
Vamos a mostrar todos las canciones que a comprado el cliente llamado Luis Rojas.
```sql
SELECT t.TrackId, t.Name
FROM  Track t
WHERE t.TrackId IN (
    SELECT il.TrackId
    FROM InvoiceLine il
    JOIN Invoice i
        ON il.InvoiceId = i.InvoiceId
    JOIN Customer c
        ON i.CustomerId = c.CustomerId
    WHERE c.FirstName = 'Luis'
      AND c.LastName = 'Rojas'
);
```
**[Enlace el archivo de la consulta 6](./query/query6.sql)**

## Query 7:
Vamos a obtener las canciones mas caras de cada album.
```sql
SELECT t.TrackId, t.Name , t.UnitPrice
FROM Track t
JOIN ( SELECT  AlbumId, MAX(UnitPrice) AS maxs
    FROM Track
    GROUP BY AlbumId) AS caras 
    ON t.AlbumId = caras.AlbumId
WHERE t.UnitPrice > caras.maxs;
```
**[Enlace el archivo de la consulta 7](./query/query7.sql)**

# Consultas con subconsultas y funciones de agregació

## Query 8:
Vamos a obtener que clientes han comprado todos los albumes del artista: Queen que es el 51.
```sql
SELECT CustomerId
FROM Invoice
WHERE InvoiceId IN (
    SELECT DISTINCT InvoiceId
    FROM InvoiceLine
    WHERE TrackId IN (
        SELECT TrackId
        FROM Track
        WHERE AlbumId IN (
            SELECT AlbumId
            FROM Album
            WHERE ArtistId = 51
        )
    )
GROUP BY CustomerId
HAVING COUNT(DISTINCT AlbumId) = (
    SELECT COUNT(AlbumId)
    FROM Album
    WHERE ArtistId = 51
));
```
**[Enlace el archivo de la consulta 8](./query/query8.sql)**

## Query 9:
Vamos a mostrar los clientes los cuales han comprado mas de 20 canciones en una sola consulta. No me sale
```sql
SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName
FROM Customer
WHERE Customer.CustomerId 
IN (SELECT Invoice.CustomerId
    FROM Invoice
    WHERE Invoice.InvoiceId 
    IN (SELECT InvoiceId
        FROM InvoiceLine
        GROUP BY InvoiceId
        HAVING COUNT(TrackId) > 20)
);
```
**[Enlace el archivo de la consulta 9](./query/query9.sql)**

## Query 10:
Vamos a mostrar las canciones que mas se han comprado pero solo el top 10.
```sql
SELECT t.TrackId, t.Name, tos.TotalPurchases
FROM Track t
JOIN (SELECT il.TrackId,COUNT(il.TrackId)
    FROM InvoiceLine il
    GROUP BY il.TrackId) AS tos
        ON t.TrackId = tos.TrackId
ORDER BY tos.TotalPurchases DESC
LIMIT 10;
```
**[Enlace el archivo de la consulta 10](./query/query10.sql)**

## Query 11:
Vamos a mostras las canciones que esten por encima de la media de las canciones.
```sql
SELECT t.TrackId,t.Name , t.Milliseconds, av.ave
FROM Track t
CROSS JOIN 
    (SELECT AVG(Milliseconds) AS ave FROM Track) AS av
WHERE t.Milliseconds > av.ave;
```
**[Enlace el archivo de la consulta 11](./query/query11.sql)**

## Query 12:
En esta consulta vamos a mostrar el número de países de clientes, géneros musicales y pistas y mostraremos con un "AS" para saber identificarlo.
```sql
SELECT 
    (SELECT COUNT(DISTINCT Country) FROM Customer) AS paises,
    (SELECT COUNT(*) FROM Genre) AS generos,
    (SELECT COUNT(*) FROM Track) AS psitas;
```
**[Enlace el archivo de la consulta 12](./query/query12.sql)**

## Query 13:
Vamos a mostrar las canciones que han sido mas vendidas de la media de ventas por canciones.
```sql
SELECT t.TrackId, t.Name, t.SalesCount, AVG(av.sal) AS av
FROM Track t
JOIN (SELECT il2.TrackId, COUNT(il2.InvoiceLineId) AS sal
     FROM InvoiceLine il2 
     GROUP BY il2.TrackId) AS av
    ON t.TrackId = av.TrackId
JOIN InvoiceLine il 
    ON t.TrackId = il.TrackId
GROUP BY  t.TrackId, t.Name, t.SalesCount
HAVING t.SalesCount > AVG(av.sal);
```
**[Enlace el archivo de la consulta 13](./query/query13.sql)**

## Query 14:

```sql

```
**[Enlace el archivo de la consulta 14](./query/query14.sql)**

## Query 15:
Vamops a calcular el descuento aplicado sobre cada factura, que solo les aremos descuento si tienen un gasto total de mas de 100€.
```sql
SELECT InvoiceId, Total,
    (SELECT 
        CASE 
            WHEN Total > 100 THEN Total * 0.1
            ELSE 0
        END
    )
FROM Invoice;
```
**[Enlace el archivo de la consulta 15](./query/query15.sql)**

## Query 16:
Vamos a clasificar a los empleados segun su cargo.
```sql
SELECT e.EmployeeId, e.FirstName, e.LastName, e.Title, l.Level
FROM Employee e
JOIN (SELECT DISTINCT Title,
    CASE 
        WHEN Title LIKE '%Manager%' THEN 'Manager'
        WHEN Title LIKE '%Assistant%' THEN 'Assistant'
        ELSE 'Empleado'
    END AS Level
    FROM Employee) AS l 
ON e.Title = l.Title;
```
**[Enlace el archivo de la consulta 16](./query/query16.sql)**

## Query 17:
Vamos a poner que clientes son "VIPS" que son que en el total de sus compras han gastado mas de 500€.
```sql
SELECT c.CustomerId, c.FirstName, c.LastName, (SELECT SUM(Total) FROM Invoice WHERE Invoice.CustomerId = c.CustomerId) AS sumtot,
    CASE 
        WHEN (SELECT SUM(Total) FROM Invoice WHERE Invoice.CustomerId = c.CustomerId) > 500 THEN 'VIP'
        ELSE 'Regular'
    END AS CustomerType
FROM Customer c;
```
**[Enlace el archivo de la consulta 17](./query/query17.sql)**

