SELECT e.EmployeeId, e.FirstName, e.LastName, e.ReportsTo, supervisor.nombre
FROM Employee e
JOIN (SELECT EmployeeId, CONCAT(FirstName, ' ', LastName) AS nombre
    FROM Employee) AS supervisor
    ON e.ReportsTo = supervisor.EmployeeId
ORDER BY e.ReportsTo, e.EmployeeId;