SELECT Track.TrackId, Track.Name
FROM Track
WHERE Track.TrackId NOT
IN (
    SELECT InvoiceLine.TrackId
    FROM InvoiceLine
);