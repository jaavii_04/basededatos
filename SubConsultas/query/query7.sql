SELECT t.TrackId, t.Name , t.UnitPrice
FROM Track t
JOIN ( SELECT  AlbumId, MAX(UnitPrice) AS maxs
    FROM Track
    GROUP BY AlbumId) AS caras 
    ON t.AlbumId = caras.AlbumId
WHERE t.UnitPrice > caras.maxs;