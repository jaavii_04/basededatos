SELECT CustomerId
FROM Invoice
WHERE InvoiceId IN (
    SELECT DISTINCT InvoiceId
    FROM InvoiceLine
    WHERE TrackId IN (
        SELECT TrackId
        FROM Track
        WHERE AlbumId IN (
            SELECT AlbumId
            FROM Album
            WHERE ArtistId = 51
        )
    )
GROUP BY CustomerId
HAVING COUNT(DISTINCT AlbumId) = (
    SELECT COUNT(AlbumId)
    FROM Album
    WHERE ArtistId = 51
));