SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName
FROM Customer
WHERE Customer.CustomerId 
IN (SELECT Invoice.CustomerId
    FROM Invoice
    WHERE Invoice.InvoiceId 
    IN (SELECT InvoiceId
        FROM InvoiceLine
        GROUP BY InvoiceId
        HAVING COUNT(TrackId) > 20)
);