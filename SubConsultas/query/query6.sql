SELECT t.TrackId, t.Name
FROM  Track t
WHERE t.TrackId IN (
    SELECT il.TrackId
    FROM InvoiceLine il
    JOIN Invoice i
        ON il.InvoiceId = i.InvoiceId
    JOIN Customer c
        ON i.CustomerId = c.CustomerId
    WHERE c.FirstName = 'Luis'
      AND c.LastName = 'Rojas'
);