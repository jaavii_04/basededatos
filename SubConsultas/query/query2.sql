SELECT DISTINCT Artist.ArtistId, Artist.Name
FROM Artist
WHERE Artist.ArtistId IN (
SELECT DISTINCT Artist.ArtistId
FROM Artist
JOIN  Album
    ON Artist.ArtistId = Album.ArtistId
JOIN Track
    ON Album.AlbumId = Track.AlbumId
WHERE
    Track.Milliseconds > 300000
);