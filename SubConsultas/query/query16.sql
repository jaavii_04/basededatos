SELECT e.EmployeeId, e.FirstName, e.LastName, e.Title, l.Level
FROM Employee e
JOIN (SELECT DISTINCT Title,
    CASE 
        WHEN Title LIKE '%Manager%' THEN 'Manager'
        WHEN Title LIKE '%Assistant%' THEN 'Assistant'
        ELSE 'Empleado'
    END AS Level
    FROM Employee) AS l 
ON e.Title = l.Title;