SELECT InvoiceId, Total,
    (SELECT 
        CASE 
            WHEN Total > 100 THEN Total * 0.1
            ELSE 0
        END
    )
FROM Invoice;