SELECT t.TrackId, t.Name, tos.TotalPurchases
FROM Track t
JOIN (SELECT il.TrackId,COUNT(il.TrackId)
    FROM InvoiceLine il
    GROUP BY il.TrackId) AS tos
        ON t.TrackId = tos.TrackId
ORDER BY tos.TotalPurchases DESC
LIMIT 10;