SELECT c.CustomerId, c.FirstName, c.LastName, (SELECT SUM(Total) FROM Invoice WHERE Invoice.CustomerId = c.CustomerId) AS sumtot,
    CASE 
        WHEN (SELECT SUM(Total) FROM Invoice WHERE Invoice.CustomerId = c.CustomerId) > 500 THEN 'VIP'
        ELSE 'Regular'
    END AS CustomerType
FROM Customer c;