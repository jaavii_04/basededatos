SELECT t.TrackId, t.Name, t.SalesCount, AVG(av.sal) AS av
FROM Track t
JOIN (SELECT il2.TrackId, COUNT(il2.InvoiceLineId) AS sal
     FROM InvoiceLine il2 
     GROUP BY il2.TrackId) AS av
    ON t.TrackId = av.TrackId
JOIN InvoiceLine il 
    ON t.TrackId = il.TrackId
GROUP BY  t.TrackId, t.Name, t.SalesCount
HAVING t.SalesCount > AVG(av.sal);