SELECT Employee.FirstName, Employee.LastName
FROM Employee
WHERE Employee.EmployeeId IN (
    SELECT DISTINCT Customer.SupportRepId
    FROM Customer
);