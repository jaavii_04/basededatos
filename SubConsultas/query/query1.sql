SELECT Playlist.PlaylistId, Playlist.Name, t.TrackId, t.Name, t.AlbumId, Album.Title, t.UnitPrice
FROM Playlist
JOIN PlaylistTrack
    ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
JOIN Track AS t
    ON PlaylistTrack.TrackId = t.TrackId
JOIN Album
    ON t.AlbumId = Album.AlbumId
WHERE Playlist.Name LIKE 'M%'
    AND (
        SELECT COUNT(*)
        FROM Track
        WHERE Track.AlbumId = Album.AlbumId
        AND UnitPrice <= t.UnitPrice
    ) <= 3
ORDER BY Album.Title, t.UnitPrice;