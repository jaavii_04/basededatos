SELECT t.TrackId,t.Name , t.Milliseconds, av.ave
FROM Track t
CROSS JOIN 
    (SELECT AVG(Milliseconds) AS ave FROM Track) AS av
WHERE t.Milliseconds > av.ave;