**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_funciones)**
# Consultas_funciones

## Query 1: Encuentra todos los clientes de Francia
Lo que hacemos es seleccionar de la tabla Customer mostrar  CustomerId, FirstName, LastName, Country, Phone, Email y seleccionaremos la tabla Customer y filtraremos por la columna Contry para indicar que el pais sea France
```sql
SELECT Chinook.Customer.CustomerId, FirstName, LastName, Country, Phone, Email
FROM Chinook.Customer
WHERE Country = "France";
```
**[Enlace el archivo de la consulta 1](./query/query1.sql)**

## Query 2: Muestra las facturas del primer trimestre de este año.
Lo que hacemos es seleccionar InvoiceId, CustomerId, InvoiceDate, Total de la tabla Invoice y vamos a filtar por el año actual que lo obtenemos con *YEAR(CURRENT_DATE)* y de ese año obtendremos las facturas del primer timestre (enero, febrero y marzo).
```sql
SELECT Chinook.Invoice.InvoiceId, CustomerId, InvoiceDate, Total
FROM Chinook.Invoice
WHERE YEAR(Chinook.Invoice.InvoiceDate) = YEAR(CURRENT_DATE)
AND MONTH(Chinook.Invoice.InvoiceDate) IN (1, 2, 3);
```
**[Enlace el archivo de la consulta 2](./query/query2.sql)**

## Query 3: Muestra todas las canciones compuestas por AC/DC.
Lo que hacemos es seleccionar de la tabla Track los campos TrackId, Name, AlbumId, GenreId y filtraremos por que el Composer que sea *"AC/DC"*.
```sql
SELECT Chinook.Track.TrackId, Name, AlbumId, GenreId
FROM Chinook.Track
WHERE Chinook.Track.Composer = "AC/DC";
```
**[Enlace el archivo de la consulta 3](./query/query3.sql)**

## Query 4: Muestra las 10 canciones que más tamaño ocupan.
Lo que hacemos es seleccionar de la tabla Track las columnas TrackId, Name, Bytes, UnitPrice y vamos a ordenarlos de manera descendente segun los Bytes que ocupa y limitaremos las 10 primeras para que no salgan todas.
```sql
SELECT Chinook.Track.TrackId, Name, Bytes, UnitPrice
FROM Chinook.Track
ORDER BY Bytes DESC
LIMIT 10;
```
**[Enlace el archivo de la consulta 4](./query/query4.sql)**

## Query 5: Muestra el nombre de aquellos países en los que tenemos clientes.
Lo que hacemos es seleccionar de la tabla Customer los paises que sean distintos.
```sql
SELECT DISTINCT Chinook.Customer.Country
FROM Chinook.Customer;
```
**[Enlace el archivo de la consulta 5](./query/query5.sql)**

## Query 6: Muestra todos los géneros musicales.
Lo que hacemos es seleccionar de la tabla Genre todas las columnas.
```sql
SELECT *
FROM Chinook.Genre;
```
**[Enlace el archivo de la consulta 6](./query/query6.sql)**

# Consultas sobre múltiples tablas

## Query 7: Muestra todos los artistas junto a sus álbumes.
Lo que hacemos es seleccionar de la tabla Artist el nombre y de la tabla Album el titulo y vamos a usar el join para relacionar las dos tablas por el ArtistId. 
```sql
SELECT Chinook.Artist.name, Chinook.Album.Title
FROM Chinook.Artist
JOIN Chinook.Album
ON Chinook.Artist.ArtistId = Chinook.Album.ArtistId;
```
**[Enlace el archivo de la consulta 7](./query/query7.sql)**

## Query 8: Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen.
Lo que hacemos es seleccionar de la tabla Employee las columnas FirstName, LastName, y luego de los supervisores e.FirstName, e.LastName los unimos con el join atraves de el campo ReportsTo los ordenaremos por fecha de nacimiento en orden descendente y solo mostraremos 5.
```sql
SELECT Chinook.Employee.FirstName, Chinook.Employee.LastName, e.FirstName, e.LastName
FROM Chinook.Employee
JOIN Chinook.Employee e
ON e.EmployeeId = Chinook.Employee.ReportsTo
ORDER BY Chinook.Employee.BirthDate DESC
LIMIT 5;
```
**[Enlace el archivo de la consulta 8](./query/query8.sql)**

## Query 9: Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación, código postal, país, importe (en este orden).
Lo que hacemos es seleccionar de la tabla Invoice las columnas InvoiceDate, FirstName, LastName, BillingAddress, PostalCode, Country, Total hacemos un join para unir las tablas atraves de el CustomerId y mostraremos los que son de Berlin.
```sql
SELECT Chinook.Invoice.InvoiceDate, FirstName, LastName, BillingAddress, PostalCode, Country, Total
FROM Chinook.Customer
JOIN Chinook.Invoice
ON Chinook.Customer.CustomerId = Chinook.Invoice.CustomerId
AND Chinook.Customer.City LIKE "Berlin";
```
**[Enlace el archivo de la consulta 9](./query/query9.sql)**

## Query 10: Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración.
Lo que hacemos es seleccionar de la tabla Playlist las columnas Name, de la tabla Track la columna Name y Milliseconds , de la tabla Album la columna Title y uniremos todas las tablas atraves del join cogiendoi el PlaylistId, AlbumIdy TrackId y haremos que comiencen por la letra C los agrupamos por titulo y ordenamos por Milliseconds.
```sql
SELECT Chinook.Playlist.Name, Track.Name, Album.Title, Track.Milliseconds
FROM Chinook.Playlist
JOIN Chinook.PlaylistTrack
    ON Chinook.Playlist.PlaylistId = Chinook.PlaylistTrack.PlaylistId
JOIN Chinook.Track
    ON Chinook.PlaylistTrack.TrackId = Chinook.Track.TrackId
JOIN Chinook.Album
    ON Chinook.Track.AlbumId = Chinook.Album.AlbumId
AND Chinook.Playlist.Name LIKE 'C%'
GROUP BY Chinook.Album.Title
ORDER BY Chinook.Track.Milliseconds;
```
**[Enlace el archivo de la consulta 10](./query/query10.sql)**

## Query 11: Muestra qué clientes han realizado compras por valores superiores a 10 €, ordenados por apellido.
Lo que hacemos es seleccionar de la tabla Customer las columnas FirstName, LastName y haremos de la tabla Invoice la suma total y cambiaremos el nombre a Total_Compras con el join juntamos las dos tablas por el CustomerId despues ponemos la condicion que no sea mayor que 10 las agrupamos por FirstName y LastName y las ordenamos de forma descendente.
```sql
SELECT Chinook.Customer.FirstName, LastName, SUM(Chinook.Invoice.Total) AS Total_Compras
FROM Chinook.Customer
JOIN Chinook.Invoice
    ON Chinook.Customer.CustomerId = Chinook.Invoice.CustomerId
AND Chinook.Invoice.Total > 10
GROUP BY Chinook.Customer.FirstName, Chinook.Customer.LastName
ORDER BY Total_Compras DESC;
```
**[Enlace el archivo de la consulta 11](./query/query11.sql)**

# Consultas con funciones de agregación

## Query 12: Muestra el importe medio, mínimo y máximo de cada factura.
Lo que hacemos es seleccionar de la tabla Invoice la columna Total y lo que hacemos es sacar el minimo maximo y media.
```sql
SELECT
    MIN(Chinook.Invoice.Total) AS "MIN",
    MAX(Chinook.Invoice.Total) AS "MAX",
    AVG(Chinook.Invoice.Total) AS "MED"
FROM Chinook.Invoice;
```
**[Enlace el archivo de la consulta 12](./query/query12.sql)**

## Query 13: Muestra el número total de artistas.
Lo que hacemos es seleccionar de la tabla Artist y contamos el numero total de artistas.
```sql
SELECT COUNT(*) AS "Nº Artistas"
FROM Chinook.Artist;
```
**[Enlace el archivo de la consulta 13](./query/query13.sql)**

## Query 14: Muestra el número de canciones del álbum “Out Of Time”.
Lo que hacemos es contar el numero de canciones del album Out Of Time que uniremos las dos tablas con un join sobre el AlbumId.
```sql
SELECT COUNT(*)
FROM Chinook.Track
JOIN Chinook.Album
    ON Chinook.Track.AlbumId = Chinook.Album.AlbumId
WHERE Album.Title LIKE "Out Of Time";
```
**[Enlace el archivo de la consulta 14](./query/query14.sql)**

## Query 15: Muestra el número de países donde tenemos clientes.
Lo que hacemos es contar el numero de paises que tenemos clientes solo contamos los que sean distintos ya que si no contamos a uno mas de una vez no estaria bien realizada la consulta.
```sql
SELECT COUNT(DISTINCT Chinook.Customer.Country)
FROM Chinook.Customer;
```
**[Enlace el archivo de la consulta 15](./query/query15.sql)**

## Query 16: Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).
Lo que hacemos es contar el numero de canciones que tiene cada genero al lado de su nombre lo que hacemos es unir las dos tablas con el join y unir las tablas atraves de el GenreId y luego las agrupamos por el nombre.
```sql
SELECT Chinook.Genre.Name, COUNT(*)
FROM Chinook.Track
JOIN Chinook.Genre
ON Chinook.Track.GenreId = Chinook.Genre.GenreId
GROUP BY Chinook.Genre.name;
```
**[Enlace el archivo de la consulta 16](./query/query16.sql)**

## Query 17: Muestra los álbumes ordenados por el número de canciones que tiene cada uno.
Lo que hacemos es contar el numero de canciones que tiene cada album uniremos dos tablas con el join atraves del AlbumID los agrupamos por el AlbumID ordenamos el total en orden descendente.
```sql
SELECT Chinook.Album.AlbumID, Chinook.Album.Title, COUNT(*) AS "Total"
FROM Chinook.Album
JOIN Chinook.Track
    ON Chinook.Album.AlbumID = Chinook.Track.AlbumID
GROUP BY Chinook.Album.AlbumID
ORDER BY Total DESC;
```
**[Enlace el archivo de la consulta 17](./query/query17.sql)**

## Query 18: Encuentra los géneros musicales más populares (los más comprados).
Lo que hacemos es contar los generos musicales que hay y cuales son los mas populares hacemos primero un join atraves del GenreId y luego otro atraves del TrackId despues agrupamos por nombre del genero y las ordenamos de manera descendente.
```sql
SELECT Chinook.Genre.Name , COUNT(*) AS "Mas_vendidas"
FROM Chinook.Genre
JOIN Chinook.Track
    ON Chinook.Genre.GenreId = Chinook.Track.GenreId
JOIN Chinook.InvoiceLine
    ON Chinook.Track.TrackId = Chinook.InvoiceLine.TrackId
GROUP BY Chinook.Genre.Name
ORDER BY Mas_vendidas DESC;
```
**[Enlace el archivo de la consulta 18](./query/query18.sql)**

## Query 19: Lista los 6 álbumes que acumulan más compras.
Lo que hacemos es contar el numero de albumes que acumulan mas compras y lo que hacemos es limitar la salida a 6 uniremos las tablas atraves del join con el AlbumID y del TrackId despues lo agrupamos por el titulo del album los ordenamos de manera descendente.
```sql
SELECT  Chinook.Album.Title , COUNT(*)
FROM Chinook.Album
JOIN Chinook.Track
    ON Chinook.Album.AlbumId = Chinook.Track.AlbumId
JOIN Chinook.InvoiceLine
    ON Chinook.Track.TrackId = Chinook.InvoiceLine .TrackId
GROUP BY Chinook.Album.Title
ORDER BY COUNT(*) DESC
LIMIT 6;
```
**[Enlace el archivo de la consulta 19](./query/query19.sql)**

## Query 20: Muestra los países en los que tenemos al menos 5 clientes.
Lo que hacemos es contar el numero de de paises para saber si tenemos mas 4 clientes (5 o mas) y lo hacemops con el GROUP BY que lo usaremos para agrupar por paises y luego usamos un HAVING para solo mostrar las que correspondan con nustra condicion.
```sql
SELECT Chinook.Customer.Country , COUNT(Country)
FROM Chinook.Customer
GROUP BY Chinook.Customer.Country
HAVING COUNT(Chinook.Customer.CustomerId) >= 5;
```
**[Enlace el archivo de la consulta 20](./query/query20.sql)**

