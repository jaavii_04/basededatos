SELECT  Chinook.Album.Title , COUNT(*)
FROM Chinook.Album
JOIN Chinook.Track
    ON Chinook.Album.AlbumId = Chinook.Track.AlbumId
JOIN Chinook.InvoiceLine
    ON Chinook.Track.TrackId = Chinook.InvoiceLine .TrackId
GROUP BY Chinook.Album.Title
ORDER BY COUNT(*) DESC
LIMIT 6;