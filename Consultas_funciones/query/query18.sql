SELECT Chinook.Genre.Name , COUNT(*) AS "Mas_vendidas"
FROM Chinook.Genre
JOIN Chinook.Track
    ON Chinook.Genre.GenreId = Chinook.Track.GenreId
JOIN Chinook.InvoiceLine
    ON Chinook.Track.TrackId = Chinook.InvoiceLine.TrackId
GROUP BY Chinook.Genre.Name
ORDER BY Mas_vendidas DESC;