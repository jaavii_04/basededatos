SELECT Chinook.Invoice.InvoiceDate, FirstName, LastName, BillingAddress, PostalCode, Country, Total
FROM Chinook.Customer
JOIN Chinook.Invoice
ON Chinook.Customer.CustomerId = Chinook.Invoice.CustomerId
AND Chinook.Customer.City LIKE "Berlin";