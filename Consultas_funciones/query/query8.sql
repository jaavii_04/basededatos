SELECT Chinook.Employee.FirstName, Chinook.Employee.LastName, e.FirstName, e.LastName
FROM Chinook.Employee
JOIN Chinook.Employee e
ON e.EmployeeId = Chinook.Employee.ReportsTo
ORDER BY Chinook.Employee.BirthDate DESC
LIMIT 5;