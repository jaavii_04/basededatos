SELECT COUNT(*)
FROM Chinook.Track
JOIN Chinook.Album
    ON Chinook.Track.AlbumId = Chinook.Album.AlbumId
WHERE Album.Title LIKE "Out Of Time";