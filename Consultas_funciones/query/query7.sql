SELECT Chinook.Artist.name, Chinook.Album.Title
FROM Chinook.Artist
JOIN Chinook.Album
ON Chinook.Artist.ArtistId = Chinook.Album.ArtistId;