SELECT Chinook.Track.TrackId, Name, Bytes, UnitPrice
FROM Chinook.Track
ORDER BY Bytes DESC
LIMIT 10;