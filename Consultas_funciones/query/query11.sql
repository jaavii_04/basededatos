SELECT Chinook.Customer.FirstName, LastName, SUM(Chinook.Invoice.Total) AS Total_Compras
FROM Chinook.Customer
JOIN Chinook.Invoice
    ON Chinook.Customer.CustomerId = Chinook.Invoice.CustomerId
AND Chinook.Invoice.Total > 10
GROUP BY Chinook.Customer.FirstName, Chinook.Customer.LastName
ORDER BY Total_Compras DESC;