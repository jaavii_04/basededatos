SELECT Chinook.Genre.Name, COUNT(*)
FROM Chinook.Track
JOIN Chinook.Genre
ON Chinook.Track.GenreId = Chinook.Genre.GenreId
GROUP BY Chinook.Genre.name;