SELECT Chinook.Album.AlbumID, Chinook.Album.Title, COUNT(*) AS "Total"
FROM Chinook.Album
JOIN Chinook.Track
    ON Chinook.Album.AlbumID = Chinook.Track.AlbumID
GROUP BY Chinook.Album.AlbumID
ORDER BY Total DESC;