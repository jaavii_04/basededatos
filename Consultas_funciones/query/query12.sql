SELECT
    MIN(Chinook.Invoice.Total) AS "MIN",
    MAX(Chinook.Invoice.Total) AS "MAX",
    AVG(Chinook.Invoice.Total) AS "MED"
FROM Chinook.Invoice;