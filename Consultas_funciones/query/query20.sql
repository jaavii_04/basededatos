SELECT Chinook.Customer.Country , COUNT(Country)
FROM Chinook.Customer
GROUP BY Chinook.Customer.Country
HAVING COUNT(Chinook.Customer.CustomerId) >= 5;