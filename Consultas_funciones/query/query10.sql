SELECT Chinook.Playlist.Name, Track.Name, Album.Title, Track.Milliseconds
FROM Chinook.Playlist
JOIN Chinook.PlaylistTrack
    ON Chinook.Playlist.PlaylistId = Chinook.PlaylistTrack.PlaylistId
JOIN Chinook.Track
    ON Chinook.PlaylistTrack.TrackId = Chinook.Track.TrackId
JOIN Chinook.Album
    ON Chinook.Track.AlbumId = Chinook.Album.AlbumId
AND Chinook.Playlist.Name LIKE 'C%'
GROUP BY Chinook.Album.Title
ORDER BY Chinook.Track.Milliseconds;