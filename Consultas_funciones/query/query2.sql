SELECT Chinook.Invoice.InvoiceId, CustomerId, InvoiceDate, Total
FROM Chinook.Invoice
WHERE YEAR(Chinook.Invoice.InvoiceDate) = YEAR(CURRENT_DATE)
AND MONTH(Chinook.Invoice.InvoiceDate) IN (1, 2, 3);