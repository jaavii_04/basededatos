-- Insert
INSERT INTO instagram_low_cost.reaccionesFotos (instagram_low_cost.reaccionesFotos.idFoto, instagram_low_cost.reaccionesFotos.idUsuario, instagram_low_cost.reaccionesFotos.idTipoReaccion)
VALUES (12, 45, 3),
       (18, 45, 3),
       (17, 45, 3),
       (13, 45, 3);

-- Consulta
SELECT COUNT(*)
FROM instagram_low_cost.reaccionesFotos
WHERE instagram_low_cost.reaccionesFotos.idFoto = 12
    AND instagram_low_cost.reaccionesFotos.idUsuario = 45
    AND instagram_low_cost.reaccionesFotos.idTipoReaccion = 3;