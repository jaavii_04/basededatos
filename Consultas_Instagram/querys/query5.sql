-- Primero el insert
INSERT INTO instagram_low_cost.reaccionesFotos (instagram_low_cost.reaccionesFotos.idFoto, instagram_low_cost.reaccionesFotos.idUsuario, instagram_low_cost.reaccionesFotos.idTipoReaccion)
VALUES (16, 15, 1),
       (16, 10, 1),
       (16, 18, 1),
       (16, 23, 1),
       (15, 15, 1);

-- Consulta
SELECT instagram_low_cost.usuarios.idUsuario, instagram_low_cost.usuarios.nombre
FROM instagram_low_cost.usuarios, instagram_low_cost.reaccionesFotos
WHERE instagram_low_cost.usuarios.idUsuario = instagram_low_cost.reaccionesFotos.idUsuario
    AND instagram_low_cost.usuarios.idRol = 3
    AND instagram_low_cost.reaccionesFotos.idTipoReaccion = 1
    HAVING COUNT(*) > 2;