-- Primero los insert
INSERT INTO instagram_low_cost.comentariosFotos (comentariosFotos.idComentarioFoto, comentariosFotos.idComentario, comentariosFotos.idFoto)
VALUES (11, 11, 12);

INSERT INTO instagram_low_cost.comentarios (comentario, idUsuario, fechaCreacion)
VALUES ("¡Qué foto más bonita!", 36, "2024-02-17 12:00:00");

-- Consulta
SELECT instagram_low_cost.comentarios.comentario
FROM instagram_low_cost.comentarios , instagram_low_cost.comentariosFotos
WHERE instagram_low_cost.comentarios.idComentario = instagram_low_cost.comentariosFotos.idComentario
    AND instagram_low_cost.comentarios.idUsuario = 36
    AND instagram_low_cost.comentariosFotos.idFoto = 12;