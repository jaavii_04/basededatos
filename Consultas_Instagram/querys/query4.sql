-- Primero los insert
INSERT INTO instagram_low_cost.fotos (instagram_low_cost.fotos.url, instagram_low_cost.fotos.descripcion, instagram_low_cost.fotos.fechaCreacion, instagram_low_cost.fotos.idUsuario)
VALUES ("https://ejemplo.com/imagen1.jpg", "Hermosa vista de la montaña al atardecer", "2024-02-17", 25),
       ("https://ejemplo.com/imagen2.jpg", "Retrato de una persona en blanco y negro", "2024-02-16", 26),
       ("https://ejemplo.com/imagen3.jpg", "Captura de un pájaro en pleno vuelo", "2024-02-15", 27);

INSERT INTO instagram_low_cost.reaccionesFotos (instagram_low_cost.reaccionesFotos.idfoto, instagram_low_cost.reaccionesFotos.idusuario, instagram_low_cost.reaccionesFotos.idTipoReaccion)
VALUES (14, 25, 4),
       (13, 26, 4),
       (15, 27, 4);

-- Consulta
SELECT instagram_low_cost.fotos.*
FROM instagram_low_cost.fotos, instagram_low_cost.reaccionesFotos
WHERE instagram_low_cost.fotos.idFoto = instagram_low_cost.reaccionesFotos.idFoto
    AND instagram_low_cost.reaccionesFotos.idUsuario = 25
    AND instagram_low_cost.reaccionesFotos.idTipoReaccion = 4;
