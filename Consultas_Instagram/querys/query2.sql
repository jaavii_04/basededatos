-- Aqui hacemos un insert primero
INSERT INTO instagram_low_cost.fotos (instagram_low_cost.fotos.idUsuario, instagram_low_cost.fotos.descripcion, instagram_low_cost.fotos.url, instagram_low_cost.fotos.fechaCreacion)
VALUES (36, "Cumpleaños", 'https://ejemplo.com/foto101.jpg', '2023-01-01 12:00:00'),
       (36, "Fiesta", 'https://ejemplo.com/foto102.jpg', '2023-01-15 15:30:00');

-- Y despues la consulta 
SELECT *
FROM instagram_low_cost.fotos
WHERE instagram_low_cost.fotos.idUsuario = 36
    AND YEAR(instagram_low_cost.fotos.fechaCreacion) = 2023
    AND MONTH(instagram_low_cost.fotos.fechaCreacion) = 01;