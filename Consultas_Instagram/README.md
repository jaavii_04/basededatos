**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Consultas_Instagram)**

# Consultas Instagram

### Ejercicio 1: Fotos del usuario con ID 36.

Aquí vamos a hacer un select de la tabla fotos del usuario 36.

```sql
SELECT *
FROM instagram_low_cost.fotos
WHERE fotos.idUsuario = 36;
```

**[Enlace el archivo de la consulta](./querys/query1.sql)**

La otra forma que lo he echo a sido hacer un *select* de las dos tablas *fotos* y *usuarios* y un where primero para que no se multiplicaran las tablas y salieran todas con todas y luego un *and* para saber las del usuario 36.

```sql
SELECT instagram_low_cost.fotos.*
FROM instagram_low_cost.fotos, instagram_low_cost.usuarios
WHERE instagram_low_cost.usuarios.idUsuario = instagram_low_cost.fotos.idUsuario
    AND instagram_low_cost.usuarios.idUsuario = 36;
```

**[Enlace el archivo de la consulta](./querys/query1_2.sql)**

---

### Ejercicio 2: Fotos del usuario con ID 36 tomadas en enero del 2023.

Vamos a realizar un *select* para obtener las Fotos del usuario con ID 36 tomadas en enero del 2023 pero comprobamos que no hay ninguna foto del id:36 tomada en enero del 2023 asique vamos a añadir unos datos vamos a añadirlos con un *insert*.

```sql
INSERT INTO instagram_low_cost.fotos (instagram_low_cost.fotos.idUsuario, instagram_low_cost.fotos.descripcion, instagram_low_cost.fotos.url, instagram_low_cost.fotos.fechaCreacion)
VALUES (36, "Cumpleaños", 'https://ejemplo.com/foto101.jpg', '2023-01-01 12:00:00'),
       (36, "Fiesta", 'https://ejemplo.com/foto102.jpg', '2023-01-15 15:30:00');
```

Después de haber realizado este insert ya tenemos unas imágenes tomadas en enero(01) del 2023 y ya podríamos realizar el *select* que lo que hacemos es coger la tabla de *fotos* y buscamos el usuario 36 del mes 01 y del año 2023.

```sql
SELECT *
FROM instagram_low_cost.fotos
WHERE instagram_low_cost.fotos.idUsuario = 36
    AND YEAR(instagram_low_cost.fotos.fechaCreacion) = 2023
    AND MONTH(instagram_low_cost.fotos.fechaCreacion) = 01;
```

**[Enlace el archivo de la consulta](./querys/query2.sql)**

---

### Ejercicio 3: Comentarios del usuario 36 sobre la foto 12 del usuario 11.

Vamos a realizar una consulta para encontrar los comentarios del usuario 36 sobre la foto 12 del usuario 11 entonces hacemos un *select* pero después de hacer el *select* me doy cuenta que no hay ninguno entonces tendremos que realizar un *insert* para insertarlo en las dos tablas.

```sql
INSERT INTO instagram_low_cost.comentariosFotos (comentariosFotos.idComentarioFoto, comentariosFotos.idComentario, comentariosFotos.idFoto)
VALUES (11, 11, 12);
```

```sql
INSERT INTO instagram_low_cost.comentarios (comentario, idUsuario, fechaCreacion)
VALUES ("¡Qué foto más bonita!", 36, "2024-02-17 12:00:00");
```

Ahora realizamos el *select* que lo que hacemos es solo mostrar los comentarios de la tabla comentarios y vamos a buscar en las tablas comentarios y comentariosFotos primero hacemos que las tablas no se multipliquen y buscamos el idusuario 36 de la tabla comentarios y el idFoto 12 de la tabla comentariosFotos.

```sql
SELECT instagram_low_cost.comentarios.comentario
FROM instagram_low_cost.comentarios , instagram_low_cost.comentariosFotos
WHERE instagram_low_cost.comentarios.idComentario = instagram_low_cost.comentariosFotos.idComentario
    AND instagram_low_cost.comentarios.idUsuario = 36
    AND instagram_low_cost.comentariosFotos.idFoto = 12;
```

**[Enlace el archivo de la consulta](./querys/query3.sql)**

---

### Ejercicio 4: Fotos que han sorprendido al usuario 25.

Vamos a realizar una consulta para encontrar las fotos que han sorprendido al usuario 25 pero como no tenemos datos vamos a tener que hacer un *insert* para cada tabla

```sql
INSERT INTO instagram_low_cost.fotos (instagram_low_cost.fotos.url, instagram_low_cost.fotos.descripcion, instagram_low_cost.fotos.fechaCreacion, instagram_low_cost.fotos.idUsuario)
VALUES ("https://ejemplo.com/imagen1.jpg", "Hermosa vista de la montaña al atardecer", "2024-02-17", 25),
       ("https://ejemplo.com/imagen2.jpg", "Retrato de una persona en blanco y negro", "2024-02-16", 26),
       ("https://ejemplo.com/imagen3.jpg", "Captura de un pájaro en pleno vuelo", "2024-02-15", 27);
```

```sql
INSERT INTO instagram_low_cost.reaccionesFotos (instagram_low_cost.reaccionesFotos.idfoto, instagram_low_cost.reaccionesFotos.idusuario, instagram_low_cost.reaccionesFotos.idTipoReaccion)
VALUES (14, 25, 4),
       (13, 26, 4),
       (15, 27, 4);
```

Ahora a la hora de realizar el select ya tendremos los datos y lo que hacemos es mostrar todo lo que tiene la tabla fotos y seleccionaremos las tablas fotos y reaccionesFotos primero hacemos que las tablas no se multipliquen y ponemos *AND* y decimos que en reaccionesFotos el idUsuario sea el 25 y que la reacción sea la numero 4 (`La reacción 4 es la reacción de sorpresa`) .

```sql
SELECT instagram_low_cost.fotos.*
FROM instagram_low_cost.fotos, instagram_low_cost.reaccionesFotos
WHERE instagram_low_cost.fotos.idFoto = instagram_low_cost.reaccionesFotos.idFoto
    AND instagram_low_cost.reaccionesFotos.idUsuario = 25
    AND instagram_low_cost.reaccionesFotos.idTipoReaccion = 4;
```

**[Enlace el archivo de la consulta](./querys/query4.sql)**

---

### Ejercicio 5: Administradores que han dado más de 2 "Me gusta".

Vamos a realizar una consulta para encontrar las fotos que han tenido más de dos "*Me gusta*" de los Administradores pero no tenemos ningún administrador que haya dado más de dos "*Me gustas*" entonces vamos a añadir unos cuantos "*Me gusta*" de los Administradores.

```sql
INSERT INTO instagram_low_cost.reaccionesFotos (instagram_low_cost.reaccionesFotos.idFoto, instagram_low_cost.reaccionesFotos.idUsuario, instagram_low_cost.reaccionesFotos.idTipoReaccion)
VALUES (16, 15, 1),
       (16, 10, 1),
       (16, 18, 1),
       (16, 23, 1),
       (15, 15, 1);
```

Ahora que ya tenemos Administradores que ya han dado más de dos "*Me gustas*" vamos a hacer el *select* para saber quien es lo que vamos a hacer es mostrar el idUsuario y el nombre del usuario y vamos a buscar en las tablas usuarios y la tablas fotos primero hacemos que las tablas no se multipliquen y buscaremos el id de rol 3 y el tipo de reacción "*Me gusta*" y luego muestro solo los que han dado más de 2.

```sql
SELECT instagram_low_cost.usuarios.idUsuario, instagram_low_cost.usuarios.nombre
FROM instagram_low_cost.usuarios, instagram_low_cost.reaccionesFotos
WHERE instagram_low_cost.usuarios.idUsuario = instagram_low_cost.reaccionesFotos.idUsuario
    AND instagram_low_cost.usuarios.idRol = 3
    AND instagram_low_cost.reaccionesFotos.idTipoReaccion = 1
    HAVING COUNT(*) > 2;
```

**[Enlace el archivo de la consulta](./querys/query5.sql)**

---

### Ejercicio 6: Número de "Me divierte" de la foto número 12 del usuario 45.

Vamos a realizar una consulta para encontrar el número de "Me divierte" de la foto número 12 del usuario 45 pero nos damos cuenta que usuario con id 45 no a tenido reacciones sobre la foto con id 12 entonces vamos a hacer un insert antes

```sql
INSERT INTO instagram_low_cost.reaccionesFotos (instagram_low_cost.reaccionesFotos.idFoto, instagram_low_cost.reaccionesFotos.idUsuario, instagram_low_cost.reaccionesFotos.idTipoReaccion)
VALUES (12, 45, 3),
       (18, 45, 3),
       (17, 45, 3),
       (13, 45, 3);
```

Ahora después de tener el inset vamos a hacer el *select* que pondremos un *COUNT( * )* y vamos a hacerlo de la tabla reaccionesFotos después vamos a filtrar del id foto 12 y que el idUsuario sea el 45 y el idTipoReaccion que sea la 3 que es *"Me divierte"*

```sql
SELECT COUNT(*)
FROM instagram_low_cost.reaccionesFotos
WHERE instagram_low_cost.reaccionesFotos.idFoto = 12
    AND instagram_low_cost.reaccionesFotos.idUsuario = 45
    AND instagram_low_cost.reaccionesFotos.idTipoReaccion = 3;
```

**[Enlace el archivo de la consulta](./querys/query6.sql)**

---

### Ejercicio 7: Número de fotos tomadas en la playa (en base al título).

Aqui lo que hago es un *select* que cuenta las filas hay en la tabla "fotos" donde la columna *"descripcion"* contiene la palabra "playa" con el `LIKE '%playa%'` lo que hacemos es buscar cualquier descripción que contenga la palabra "playa" tanto sea como delante como detras.

```sql
SELECT COUNT(*)
FROM instagram_low_cost.fotos
WHERE instagram_low_cost.fotos.descripcion 
    LIKE '%playa%';
```

**[Enlace el archivo de la consulta](./querys/query7.sql)**

---

### Base de datos

**[Aqui](./instagram_low_cost.sql)** tenemos la base de datos que he utilizado para las consultas.

---