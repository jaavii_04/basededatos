#  Reto 3: Consultas básicas con JOIN

**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/blob/main/Consultas_basicas_Join/)**

## Ejercicio 1: Lista todas las películas del videoclub junto al nombre de su género.
Seleccionamos el Pelicula titulo y la descripcion del genero.UTilizamos el *JOIN* para combinar las dos tablas basandonos en las celdas codiGenere de las dos tablas.
```sql
SELECT PELICULA.Titol , GENERE.Descripcio
FROM videoclub.PELICULA
JOIN GENERE ON PELICULA.codiGenere = GENERE.codiGenere;
```

**[Enlace el archivo de la consulta](./querys/query1.sql)**

## Ejercicio 2: Lista todas las facturas de María.
Selecionamos todas las columnas de ls tabla *Factura* y *Client* donde el dni de la factura sea igual a el dni del cliente y que comienze por **Maria**.
```sql
SELECT *
FROM FACTURA
JOIN CLIENT ON FACTURA.DNI = CLIENT.DNI
Where CLIENT.Nom LIKE "Maria%";
```

**[Enlace el archivo de la consulta](./querys/query2.sql)**

## Ejercicio 3: Lista las películas junto a su actor principal.
Selecionamos el titulo de las peliculas y los nombres de los actores ponemos el join para combinar las dos tablas pelicula y actor basandones en el codigo del actor.
```sql
SELECT PELICULA.Titol, ACTOR.Nom
FROM PELICULA
JOIN ACTOR ON PELICULA.CodiActor = ACTOR.CodiActor;
```

**[Enlace el archivo de la consulta](./querys/query3.sql)**

## Ejercicio 4: Lista películas junto a todos los actores que la interpretaron.
Seleccionamos el titulo de las peliculas y el nombre del actor y combinaremos con el join las tablas pelicula y interpretada y combinamos esta con actores basandome en la en las codipeli.
```sql
SELECT PELICULA.Titol, ACTOR.Nom
FROM PELICULA
JOIN INTERPRETADA
JOIN ACTOR ON PELICULA.CodiPeli = INTERPRETADA.CodiPeli
AND INTERPRETADA.CodiActor = ACTOR.CodiActor;
```
**[Enlace el archivo de la consulta](./querys/query4.sql)**

## Ejercicio 5: Lista ID y nombres de las películas junto a los ID y nombres de sus segundas partes.
Seleccionamos el codigo de pelicula  y el titulo y utilizamos el join para juntar la misma tabla para juntar el codi de la peli con la segunda parte.
```sql
SELECT PELICULA.CodiPeli, PELICULA.Titol
FROM videoclub.PELICULA
JOIN videoclub.PELICULA P on P.CodiPeli = videoclub.PELICULA.SegonaPart;
```

**[Enlace el archivo de la consulta](./querys/query5.sql)**