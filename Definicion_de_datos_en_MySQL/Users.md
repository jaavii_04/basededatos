**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/blob/main/Definicion_de_datos_en_MySQL/Users.md)**
# Control de Acceso en MySQL

## 1. Registrar nuevos usuarios, modificarlos y eliminarlos.

### Registrar Nuevos Usuarios
Para crear usuarios en mysql:

```sql
CREATE USER 'user'@'%' IDENTIFIED BY 'contra';
```

### Modificar Usuarios
Podremos modificar usuarios:

```sql
ALTER USER 'user'@'%' IDENTIFIED BY 'contra2';
```

```sql
RENAME USER 'user'@'%' TO 'user2'@'%';
```

### Eliminar Usuarios
Para eliminar un usuario:

```sql
DROP USER 'user2'@'%';
```

## 2. Autentican los usuarios y qué opciones de autenticación nos ofrece el SGBD.

### Opciones de Autenticación
MySQL nos ofrede unas cuantas:

- **Autenticación por contraseña**: Es la mas utilizada `CREATE USER`.
- **Plugins de autenticación**: MySQL soporta plugins como `caching_sha2_password`, `mysql_native_password`...

Como crearlo con el plugin:

```sql
CREATE USER 'user'@'%' IDENTIFIED WITH 'mysql_native_password' BY 'contra';
```

## 3. Mostrar los usuarios existentes y sus permisos.

### Mostrar Usuarios
```sql
SELECT user, host FROM mysql.user;
```

### Mostrar Permisos de un Usuario
```sql
SHOW GRANTS FOR 'user'@'%';
```

## 4. Permisos puede tener un usuario y qué nivel de granularidad nos ofrece el MySQL.

### Tipos de Permisos
MySQL ofrece una amplia gama de permisos que pueden ser otorgados a los usuarios:

- **Permisos administrativos**: `GRANT OPTION`, `RELOAD`, `SHUTDOWN`, ...
- **Permisos de base de datos**: `CREATE`, `DROP`, `INDEX`, `ALTER`, ...
- **Permisos de tabla**: `SELECT`, `INSERT`, `UPDATE`, `DELETE`, ...

### Niveles de Granularidad
Los permisos pueden ser otorgados a varios niveles de granularidad:

- **Global**: Afecta a todas las bases de datos en el servidor.
- **Base de datos**: Afecta solo a una base de datos específica.
- **Tabla**: Afecta solo a una tabla específica.
- **Columna**: Afecta solo a columnas específicas dentro de una tabla.

## 5. Permisos Necesarios para la Gestión de Usuarios

Para poder gestionar usuarios deberemos de tener permisos sobre todas las tablas.

```sql
GRANT CREATE USER ON *.* TO 'userAdmin'@'%';
GRANT DROP USER ON *.* TO 'userAdmin'@'%';
GRANT GRANT OPTION ON *.* TO 'userAdmin'@'%';
```

## 6. Agrupación de Usuarios

### Roles en MySQL

Nos permite poder crear Roles a los usuarios. En cada rol asignamos permisos y se lo asociaremos a ese usuario que .

### Crear y Asignar Roles
Crear el rol:
```sql
CREATE ROLE 'nomRol';
```

Asignar privilegios a un rol:
```sql
GRANT SELECT, INSERT ON *.* TO 'nomRol';
```

Asignar un rol a un usuario:
```sql
GRANT 'nomRol' TO 'user'@'%';
```

## 7. comandos usamos para gestionar los usuarios y sus permisos.

### Mostrar Usuarios
```sql
SELECT User, Host FROM mysql.user;
```

### Mostrar Permisos
```sql
SHOW GRANTS FOR 'user'@'%';
```

### Asignar Permisos
```sql
GRANT SELECT, INSERT ON PrintPro.* TO 'user'@'%';
```

### Revocar Permisos

Para quitar los permisos 

```sql
REVOKE SELECT, INSERT ON PrintPro.* FROM 'user'@'%';
```
