# Definicion Base de datos

## Como conectarse a una base de datos desde (CLI) linea de comandos.
```bash
mysql -u root -p -h 192.168.5.8 -P 33006
```

## Para mostrar las tablas de una base de datos.
```sql
SHOW TABLES FROM Chinook;
```
Tambien tenemos esta opcion.
```sql
USE Chinook;
SHOW TABLES;
```


## Para mostrar las columas que tiene cada tabla.
```sql
SHOW COLUMNS FROM Album;
```
Tambien tenemos esta manera mas simple.
```sql
DESCRIBE Album;
```
## Saber en la base de datos que esta seleccionada.
```sql
SELECT DATABASE();
```

## Saber si tenemos el autocommit encendido
```sql
SELECT @@autocommit;
```
si nos devuelve 1 estaria encendido y en 0 apagado.

y como cambiarlo
```sql
SET autocommit=0;
```

## Para que sirve el autocommit
Cuando tenemos el autocommit desactivado podemos hacer por ejemplo una ttransaccion que no llege al estado de pagado que empieze pero jamas acabe entonces no deberiamos de dar el pedido como pagado asi que podriamos volver al autocommit.
Por lo tanto el autocommit nos valdra para poder volver al estado del commit anterior eso si nunca podremos volver 2 atras simepre al ultimo y si hacemos commit tampoco podremos volver atras.

## Como volver al estado anterior antes de hacer el commit
```sql
ROLLBACK;
```

## Como hacer un commit
```sql
COMMIT;
```

# Creacion de base de datos

## Crear base de datos
```sql
CREATE SCHEMA Prueba;
```

## Crear tablas para Prueba.
Creacion de la tabla de Pasajeros
```sql
CREATE TABLE Pasajeros (
  id_pasajero int NOT NULL AUTO_INCREMENT,
  numero_pasaporte varchar(10) NOT NULL,
  nombre_pasajero varchar(50) NOT NULL,
  PRIMARY KEY (id_pasajero)
);
```

Creacion de la tabla Vuelos
```sql
CREATE TABLE Vuelos (
  id_vuelo int NOT NULL AUTO_INCREMENT,
  numero_vuelo varchar(10) NOT NULL,
  origen varchar(50) NOT NULL,
  destino varchar(50) NOT NULL,
  fecha datetime NOT NULL,
  capacidad int NOT NULL,
  PRIMARY KEY (id_vuelo)
);
```


```sql
CREATE TABLE `Reservas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_vuelo` int NOT NULL,
  `id_pasajero` int NOT NULL,
  `n_asiento` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Reservas_1` (`id_vuelo`),
  KEY `Reservas_2` (`id_pasajero`),
  CONSTReservasNT `Reservas_1` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`),
  CONSTRAINT `Reservas_2` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`)
);
```

## Explicacion de elecciones en las tablas 

- Tabla de Pasajeros:
  * id_pasajero: Lo elegimos como numero y no null para que cada pasajero tenga un numero unico y es auto_increment para que se sume solo y no tengamos que decirle cada vez un numero mayor.
  * numero_pasaporte: Lo elegimos como varchar porque el pasaporte puede contener tanto numeros como letras lo ponemos como no null para que siempre tengamos el pasaporte del pasajero
  * nombre_pasajero: Lo elegimos como varchar con 50 caracteres por si el nombre es largo y que no sea null para tener siempre su nombre.
- Tabla de Vuelos:
  * id_vuelo: Lo elegimos como numero y no null para que cada vuelo tenga un numero unico y es auto_increment para que se sume solo y no tengamos que decirle cada vez un numero mayor.
  * numero_vuelo: Lo elegimos como varchar porque el numero de vuelo  puede contener tanto numeros como letras lo ponemos como no null porque siempre necesitamos saber el nombre de cada vuelo.
  * origen: Lo elegimos como varchar porque el origen es un conjunto de letras para dar el origen y que nunca sea null.
  * destino: Lo elegimos como varchar porque el destino es un conjunto de letras para dar el destino y que nunca sea null.
  * fecha: Lo elegimos como DateTime para poder guardar la hora y la fecha nuenca debe ser nulo porque el vuelo debe tener un dia y hora.
  * capacidad: Lo elegimos como numero y no null para que la capacidad de un vuelo tenga siempre un minimo de capacidad y que sea un numero porque la capacidad se describe como numero. 
- Tabla de Reservas:
  * id: Lo elegimos como numero y no null para saber el id de cada reserva que sera unico.
  * id_vuelo: Lo elegimos como numero y no null para que el id_vuelo siempre tenga un vuelo y le ponemos una clave ajena a vuelos para que siempre exista el vuelo.
  * id_pasajero: Lo elegimos como numero y no null para que el id_pasajero siempre tenga un vuelo y le ponemos una clave ajena a vuelos para que siempre exista el pasajero.
  * n_asiento: Lo elegimos como numero y no null para que el numero de asiento sea siempre un numero y que no sea vacio porque tiene que viajar sentado en una asiento.
