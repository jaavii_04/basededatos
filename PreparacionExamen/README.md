Query 1: Encuentra todos los clientes de Francia
```sql
SELECT FirstName, LastName, Address FROM Customer WHERE country = "FRANCE";
```
Query 2: Muestra las facturas del primer trimestre de este año.
```sql
SELECT FirstName, LastName, InvoiceId, InvoiceDate
FROM Invoice
JOIN Chinook.Customer C on C.CustomerId = Invoice.CustomerId
WHERE MONTH(InvoiceDate) between 1 AND 3
AND YEAR(InvoiceDate) = YEAR(CURRENT_DATE);
```

Query 3: Muestra todas las canciones compuestas por AC/DC.
```sql
SELECT Chinook.Track.TrackId, Name, AlbumId, GenreId
FROM Track WHERE Composer = "AC/DC";
```
Query 4: Muestra las 10 canciones que más tamaño ocupan.
```sql
SELECT  trackid, name, albumid, composer, bytes/1000000000
FROM Track
ORDER BY Bytes DESC
LIMIT 10;
```
Query 5: Muestra el nombre de aquellos países en los que tenemos clientes.
```sql
SELECT DISTINCT(Country) FROM Customer;
```
Query 6: Muestra todos los géneros musicales.
```sql
SELECT Name FROM Genre;
```
-- Consulta con cuantas canciones tiene cada genero
```sql
SELECT Name , (SELECT COUNT(*) FROM Track WHERE Genre.GenreId = GenreId)
FROM Genre;
```
--- 

Consulta 1
Obtener las canciones con una duración superior a la media.
```sql
SELECT trackid, name, milliseconds FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds) FROM Track);
```
Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".
```sql
SELECT Invoice.*
FROM Invoice
JOIN Chinook.Customer C on C.CustomerId = Invoice.CustomerId
WHERE Email = "emma_jones@hotmail.com"
ORDER BY InvoiceId DESC;
```

```sql
SELECT *
FROM Invoice
WHERE CustomerId = (SELECT CustomerId FROM Customer WHERE Email = "emma_jones@hotmail.com")
ORDER BY InvoiceId DESC
LIMIT 5;
```

Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.
```sql
SELECT A.Name, AlbumId, Name ,(SELECT COUNT(*) From Track WHERE Album.ArtistId = Track.AlbumId)
FROM Album
JOIN Chinook.Artist A on A.ArtistId = Album.ArtistId
Where 15 < (SELECT COUNT(*) From Track WHERE Album.ArtistId = Track.AlbumId);
```

