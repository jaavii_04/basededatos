SELECT *
FROM Track
where GenreId IN (
  SELECT GenreId
  FROM Chinook.Track 
  group by GenreId
  having COUNT(*)
  )
AND GenreId = 1;