SELECT *
FROM Customer
WHERE CustomerId IN (
    SELECT DISTINCT CustomerId
    FROM Invoice
    WHERE Total > 20
);