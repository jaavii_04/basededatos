SELECT EmployeeId, FirstName, LastName,
    (
        SELECT COUNT(CustomerId)
        FROM Customer
        WHERE Employee.EmployeeId = Customer.SupportRepId
    )
FROM Employee
WHERE Title = 'Sales Support Agent';