SELECT *
FROM Playlist
WHERE PlaylistId IN (
    SELECT DISTINCT PlaylistId
    FROM PlaylistTrack
    JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
    JOIN Genre ON Track.GenreId = Genre.GenreId
    WHERE Genre.Name = 'Reggae'
);