SELECT * 
FROM (
	SELECT COUNT(*), AlbumId
	FROM Chinook.Track 
	group by AlbumId
	) AS N
JOIN Album USING(AlbumId);