SELECT CustomerId, FirstName, LastName,
    (
        SELECT SUM(Total)
        FROM Invoice
        WHERE Customer.CustomerId = Invoice.CustomerId
    ) AS Total_Gastado
FROM Customer;