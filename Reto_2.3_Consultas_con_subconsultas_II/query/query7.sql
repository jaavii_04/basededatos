SELECT Album.Title AS Nombre_Album, SUM(Track.Milliseconds) AS Duracion_Total
FROM Album
JOIN Track ON Album.AlbumId = Track.AlbumId
GROUP BY Album.AlbumId, Album.Title
HAVING SUM(Track.Milliseconds) > (
    SELECT AVG(DuracionTotal)
    FROM (
        SELECT SUM(Track.Milliseconds) AS DuracionTotal
        FROM Album
        JOIN Track ON Album.AlbumId = Track.AlbumId
        GROUP BY Album.AlbumId
    ) AS Subconsulta
);