SELECT EmployeeId, FirstName, LastName, (
        SELECT SUM(Total)
        FROM Invoice
        JOIN Chinook.Customer USING(CustomerId)
    )
FROM Employee;