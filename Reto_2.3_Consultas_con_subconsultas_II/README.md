**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/tree/main/Reto_2.3_Consultas_con_subconsultas_II)**

# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)

Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Consulta 1
Obtener las canciones con una duración superior a la media.
```sql
SELECT *
FROM Track
WHERE Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
);
```
**[Enlace el archivo de la consulta 1](./query/query1.sql)**

### Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".

```sql
SELECT *
FROM Invoice
WHERE CustomerId = (
    SELECT CustomerId
    FROM Customer
    WHERE Email = 'emma_jones@hotmail.com')
LIMIT 5;
```
**[Enlace el archivo de la consulta 2](./query/query2.sql)**


## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.

```sql
SELECT *
FROM Playlist
WHERE PlaylistId IN (
    SELECT DISTINCT PlaylistId
    FROM PlaylistTrack
    JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
    JOIN Genre ON Track.GenreId = Genre.GenreId
    WHERE Genre.Name = 'Reggae'
);
```
**[Enlace el archivo de la consulta 3](./query/query3.sql)**

### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20€.
```sql
SELECT *
FROM Customer
WHERE CustomerId IN (
    SELECT DISTINCT CustomerId
    FROM Invoice
    WHERE Total > 20
);
```
**[Enlace el archivo de la consulta 4](./query/query4.sql)**

### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.
```sql
SELECT *
FROM Album
JOIN Artist USING (ArtistId)
WHERE AlbumId IN (
	SELECT AlbumId
    FROM Track
    GROUP BY AlbumId
    HAVING COUNT(*) > 15
);
```
**[Enlace el archivo de la consulta 5](./query/query5.sql)**
### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.
```sql
SELECT * FROM Album
WHERE AlbumId IN (
	SELECT AlbumId
	FROM Track
	GROUP BY AlbumId
	HAVING COUNT(*) > (
		SELECT AVG(N_Canciones) FROM (
				SELECT AlbumId, COUNT(*) AS N_Canciones
					FROM Track
					GROUP BY AlbumId
						) AS Album_NCanciones
					)
	);
```
**[Enlace el archivo de la consulta 6](./query/query6.sql)**

### Consulta 7
Obtener los álbumes con una duración total superior a la media.

```sql
SELECT Album.Title AS Nombre_Album, SUM(Track.Milliseconds) AS Duracion_Total
FROM Album
JOIN Track ON Album.AlbumId = Track.AlbumId
GROUP BY Album.AlbumId, Album.Title
HAVING SUM(Track.Milliseconds) > (
    SELECT AVG(DuracionTotal)
    FROM (
        SELECT SUM(Track.Milliseconds) AS DuracionTotal
        FROM Album
        JOIN Track ON Album.AlbumId = Track.AlbumId
        GROUP BY Album.AlbumId
    ) AS Subconsulta
);
```
**[Enlace el archivo de la consulta 7](./query/query7.sql)**

### Consulta 8
Canciones del género con más canciones.

```sql
SELECT *
FROM Track
where GenreId IN (
  SELECT GenreId
  FROM Chinook.Track 
  group by GenreId
  having COUNT(*)
  )
AND GenreId = 1;
```
**[Enlace el archivo de la consulta 8](./query/query8.sql)**

### Consulta 9
Canciones de la _playlist_ con más canciones.

```sql

```
**[Enlace el archivo de la consulta 9](./query/query9.sql)**


## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]

### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

```sql
SELECT CustomerId, FirstName, LastName,
    (
        SELECT SUM(Total)
        FROM Invoice
        WHERE Customer.CustomerId = Invoice.CustomerId
    ) AS Total_Gastado
FROM Customer;
```
**[Enlace el archivo de la consulta 10](./query/query10.sql)**

### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
```sql
SELECT EmployeeId, FirstName, LastName,
    (
        SELECT COUNT(CustomerId)
        FROM Customer
        WHERE Employee.EmployeeId = Customer.SupportRepId
    )
FROM Employee
WHERE Title = 'Sales Support Agent';
```
**[Enlace el archivo de la consulta 11](./query/query11.sql)**

### Consulta 12
Ventas totales de cada empleado.

```sql
SELECT EmployeeId, FirstName, LastName, (
        SELECT SUM(Total)
        FROM Invoice
        JOIN Chinook.Customer USING(CustomerId)
    )
FROM Employee;
```
**[Enlace el archivo de la consulta 12](./query/query12.sql)**

### Consulta 13
Álbumes junto al número de canciones en cada uno.

```sql
SELECT * 
FROM (
	SELECT COUNT(*), AlbumId
	FROM Chinook.Track 
	group by AlbumId
	) AS N
JOIN Album USING(AlbumId);
```
**[Enlace el archivo de la consulta 13](./query/query13.sql)**


### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.
```sql

```
**[Enlace el archivo de la consulta 14](./query/query14.sql)**


## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/


<!--
HAVING ??
GROUP BY ??
-->
