# Consultas basicas II

**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/blob/main/Consultas_basicas_II)**

# Empresa

## 1. Muestre los productos (código y descripción) que comercializa la empresa.
Vamos a seleccionar todas las columnas de la tabla *PRODUCTE* y nos devolvera todo lo que ahi dentro de la misma.
```sql
SELECT *
FROM empresa.PRODUCTE;
```

**[Enlace el archivo de la consulta](./querys/query1.sql)**

## 2. Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción.
Vamos a seleccionar todas las filas de la tabla *PRODUCTE* y vamos a poner la condición de que contengan la palabra **TENNIS** en la descripción. Lo hacemos con LIKE, que le decimos con % que haya algo delante o detrás, así que contiene **TENNIS**.
```sql
SELECT *
FROM empresa.PRODUCTE
WHERE empresa.PRODUCTE.DESCRIPCIO LIKE '%TENNIS%';
```

**[Enlace el archivo de la consulta](./querys/query2.sql)**

## 3. Muestre el código, nombre, área y teléfono de los clientes de la empresa.
Vamos a seleccionar las columnas *CLIENT_COD, NOM, AREA, TELEFON* de la tabla *CLIENT* y mostramos todo ya que no tenemos condicion.
```sql
SELECT empresa.CLIENT.CLIENT_COD, NOM, AREA, TELEFON
FROM empresa.CLIENT;
```

**[Enlace el archivo de la consulta](./querys/query3.sql)**

## 4. Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.
Vamos a seleccionar las columnas *CLIENT_COD, NOM, AREA y TELEFON* de la tabla *CLIENT* y aplicamos la condicion de que muestre todas las que no sea el **AREA** 636, Vamos que sea diferente a dicho numero.
```sql
SELECT empresa.CLIENT.CLIENT_COD, NOM, AREA, TELEFON
FROM empresa.CLIENT
WHERE CLIENT.AREA != 636;
```

**[Enlace el archivo de la consulta](./querys/query4.sql)**

## 5. Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío).
Vamos a seleccionar las columnas *COM_NUM, COM_DATA, DATA_TRAMESA* de la tabla *COMANDA* y al no poner ninguna condición mostrata todas las filas de la tabla *COMANDA*.
```sql
SELECT empresa.COMANDA.COM_NUM, COM_DATA, DATA_TRAMESA
FROM empresa.COMANDA;
```

**[Enlace el archivo de la consulta](./querys/query5.sql)**

# Videoclub

## 6. Lista de nombres y teléfonos de los clientes.
Vamos a seleccionar las columnas *CLIENT.nom, telefon*  de la tabla *CLIENT* y al no poner ninguna condición mostraremos todas las filas de la tabla.
```sql
SELECT videoclub.CLIENT.nom, telefon
FROM videoclub.CLIENT;
```

**[Enlace el archivo de la consulta](./querys/query6.sql)**

## 7. Lista de fechas e importes de las facturas.
Vamos a seleccionar las columnas *Data, Import*  de la tabla *FACTURA* y al no poner ninguna condición mostraremos todas las filas de la tabla.
```sql
SELECT videoclub.FACTURA.Data, Import
FROM videoclub.FACTURA;
```

**[Enlace el archivo de la consulta](./querys/query7.sql)**

## 8. Lista de productos (descripción) facturados en la factura número 3.
Vamos a seleccionar las columna *Descripcio* de la tabla *DETALLFACTURA* y le vamos a poner la condición que si el *CodiFactura* es igual a 3 mostramos la descripcion.
```sql
SELECT videoclub.DETALLFACTURA.Descripcio
FROM videoclub.DETALLFACTURA
WHERE videoclub.DETALLFACTURA.CodiFactura = 3;
```

**[Enlace el archivo de la consulta](./querys/query8.sql)**

## 9. Lista de facturas ordenada de forma decreciente por importe.
Vamos a seleccionar todas las columnas de la tabla *FACTURA* y vamos a ordenar los resultados de la consulta de manera descendente con el importe.
```sql
SELECT *
FROM videoclub.FACTURA
ORDER BY videoclub.FACTURA.Import DESC;
```

**[Enlace el archivo de la consulta](./querys/query9.sql)**

## 10. Lista de los actores cuyo nombre comience por X.
Vamos a seleccionar todas las columnas de la tabla *ACTOR* y le vamos a poner la condición que sea que empieze por la letra **X** y vamos a usar el *LIKE* para decir que si empieza por X y despues tenga lo que sea indiferentemente.
```sql
SELECT *
FROM videoclub.ACTOR
WHERE videoclub.ACTOR.Nom LIKE 'X%'
```

**[Enlace el archivo de la consulta](./querys/query10.sql)**