**[Enlace a mi repositorio](https://gitlab.com/jaavii_04/basededatos/-/blob/main/Recapitulacion/RECAP.md)**

# Unidad C0: Recapitulación

Javier Puertas Rosello.

## Introduccion
Hemos hablado del origen de las bases de datos y como funcionan las base de datos y como se conecta con el modelo cliente servidor ademas vemos como son las consultas y las mas usadas hoy en dia.

## Concepto y origen de las bases de datos
Las Bases de datos son sistemas de almacenamiento que sirven para tener organizado la informacion y acceder facilmente y es mucho mas eficiente ya que todo esta ordenado y estan relacionadas entre si. 

Tratan de resolver el problema de manupular la informacion ya que si no seria muy lenta.

## Sistemas de gestión de bases de datos
Es un sw(software) que hace que sea mas facil la creacion administracion y manipulacion de las bases de datos y tiene caracteristicas como:
* Acceso rapido
* Capacidad de respaldo y recuperacion
* Seguridad para quien accede a los datos
* Procesamiento eficiente

### Ejemplos de sistemas de gestión de bases de datos
Hoy en dia las DBMS que mas se utilizan son:
1. MySQL
2. Microsoft SQL Server
3. Oracle Database
4. ProgreSQL

De ellas son software libre:
1. PosgreSQL
2. MariaDB
3. MySQL

Todas las DBMS usan el cliente modelo servidor.

## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor? ¿Qué ventajas tiene desacoplar al DBMS del cliente? ¿En qué se basa el modelo cliente-servidor?

* __Cliente__: Es el ordenador/software desde el que se realizaran las consultas
* __Servidor__: Es el ordenador/software que proporcionara el servicio.
* __Red__: Sitio mediante el que se conectan el cliente y el  servidor 
* __Puerto de escucha__: Por el cual el cliente accede a ese servicio.
* __Petición__: Es cuando el cliente demanda algo al servidor(Consulta).
* __Respuesta__: Es el mensaje que devuelve cuando recibe la peticion.

## SQL
Es un lenguaje para administrar, manipular y consultar bases de datos relacionales. Desde este lenguaje podemos insertar, actualizar, borrar... 

### Instrucciones de SQL

#### DDL (Data Definition Language)
REATE (crear), ALTER (alterar) y DROP (eliminar)
definir y modificar la estructura de tablas
#### DML (Data Manipulation Language)
INSERT (insertar), UPDATE (actualizar) y DELETE (eliminar)
para agregar, modificar y eliminar datos en las tablas.
#### DCL (Data Control Language)
GRANT (otorgar) y REVOKE (revocar)
controlar los permisos de acceso y seguridad
#### TCL (Transaction Control Language)
COMMIT (confirmar), ROLLBACK (deshacer) y SAVEPOINT (punto de guardado)
para controlar el inicio, finalización y reversión de transacciones en la base de datos.

## Bases de datos relacionales
Una base de datos relacionales son aqullas que estan organizadas en tablas y relacionadas entre si.
Sus ventajas son integridad de los datos, Estructura organizada y clara, Flexibilidad, Seguridad...

* __Relación (tabla)__: Cada tabla tiene un nombre unico y esta compuesta 
* __Atributo/Campo (columna)__: Los atributos, también conocidos como campos o columnas
* __Registro/Tupla (fila)__: Un registro o tupla es una instancia individual de datos en una tabla. 

